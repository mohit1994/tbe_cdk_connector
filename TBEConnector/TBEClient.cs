﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using TBEConnector.models;

namespace TBEConnector
{
    public class TBEClient
    {
        private const string BaseUrl = "https://tbe.taleo.net/MANAGER/dispatcher/api/v1/serviceUrl/{0}";
        private const string LogInUrl = "{0}/login?orgCode={1}&userName={2}&password={3}";
        private const string LogOutUrl = "{0}logout";
        private string hostUrl = null;
        private string getAccessToken = null;
        
        public void CheckLoginUrlAndAccessToken(string companyCode , string userName , string password)
        {
            string response = GetLogInUrl(BaseUrl , companyCode);
            hostUrl = GetUrlFromResponse(response);
            string getUrl = GetUrlFromResponse(response);
            string accessToken = GetAccessToken(LogInUrl , getUrl , companyCode , userName , password);
            getAccessToken = GetAccessTokenFromResponse(accessToken);
        }

        public string GetLogInUrl(string baseUrl , string companyCode)
        {
            var uri = string.Format(baseUrl, companyCode);
            return GETRequest(uri);
        }
         
        public string GetAccessToken(string loginUrl , string getUrl , string companyCode, string userName, string password)
        {
            var uri = string.Format(loginUrl, getUrl , companyCode , userName , password);
            string data  = "";
            return PostRequest(uri, data);
        }

        public string GetUrlFromResponse(string url)
        {
            UrlResponseData data = JsonConvert.DeserializeObject<UrlResponseData>(url);
            return data.response.URL;
        }

        public string GetAccessTokenFromResponse(string url)
        {
            UrlResponseData data = JsonConvert.DeserializeObject<UrlResponseData>(url);
            return data.response.authToken;
        }
        public void GetLogOut()
        {
            var uri = string.Format(LogOutUrl, hostUrl);
            PostRequest(uri, "");
        }

        public string GETRequest(string uri)
        {
            string response = "";
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(uri);
            if (!string.IsNullOrEmpty(getAccessToken))
            {
                httpWebRequest.Headers.Add("Authorization", string.Format("Basic {0}", getAccessToken));
            }      
            httpWebRequest.Method = "GET";
            httpWebRequest.ContentType = "application/json";
            var httpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            var responseStream = httpWebResponse.GetResponseStream();
            if (responseStream != null)
            {
                var streamReader = new StreamReader(responseStream);
                response = streamReader.ReadToEnd();
            }
            if (responseStream != null) responseStream.Close();
            httpWebResponse.Close();
            return response;
        }

        public string PostRequest(string url, string data)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            if (!string.IsNullOrEmpty(getAccessToken))
            {
                request.Headers.Add("Authorization", string.Format("Basic {0}", getAccessToken));
            }
            request.Method = "POST";
            request.ContentType = "application/json";
            byte[] byteArray = Encoding.UTF8.GetBytes(data);
            Stream dataStream = request.GetRequestStream();
            dataStream.Write(byteArray, 0, byteArray.Length);
            dataStream.Close();

            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            dataStream = response.GetResponseStream();
            StreamReader reader = new StreamReader(dataStream);
            string responseFromServer = reader.ReadToEnd();
            reader.Close();
            dataStream.Close();
            response.Close();
            return responseFromServer;
        }

    }
}
