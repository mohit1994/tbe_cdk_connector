﻿using Scribe.Core.ConnectorApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Scribe.Core.ConnectorApi.Metadata;
using System.Reflection;
using TBEConnector.models;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace TBEConnector
{
    class MetadataProvider: IMetadataProvider
    {
        #region Member Properties

        /// <summary>
        /// Contains the collection of Entity types. Used to generate Metadata for Scribe Online.
        /// </summary>
        private Dictionary<string, Type> EntityCollection = new Dictionary<string, Type>();

        private const string CANDIDATE_ENTITY = "Candidate";
        private const string EDUCATION_ENTITY = "Education";
        private const string WORKHISTORY_ENTITY = "WorkHistory";
        #endregion

        /// <summary>
        /// Creates an instance of the MetadataProvider and prepopulates a dictionary with the Entity types we want to register.
        /// </summary>
        public MetadataProvider()
        {

            //Fill the Entity collection. We'll use this to create metadata when Scribe Online asks for it:
            EntityCollection = PopulateEntityCollection();

        }
        
        #region IMetadataProvider Implementation

        public void ResetMetadata()
        {
        }

        public IEnumerable<IActionDefinition> RetrieveActionDefinitions()
        {
            var actionDefinitions = new List<IActionDefinition>();
          var createDef = new ActionDefinition
            {
              SupportsInput = true,
              KnownActionType = KnownActions.Create,
              SupportsBulk = false,
              FullName = KnownActions.Create.ToString(),
              Name = KnownActions.Create.ToString(),
              Description = string.Empty
          };

            actionDefinitions.Add(createDef);

            var upsertDef = new ActionDefinition
            {
                KnownActionType = KnownActions.UpdateInsert,
                SupportsBulk = false,
                SupportsConstraints = false,
                SupportsInput = true,
                SupportsMultipleRecordOperations = false,
                SupportsRelations = false,
                SupportsSequences = false,
                FullName = KnownActions.UpdateInsert.ToString(),
                Name = KnownActions.UpdateInsert.ToString(),
                Description = string.Empty,
                SupportsLookupConditions = false,
            };

            actionDefinitions.Add(upsertDef);

            var queryDef = new ActionDefinition
            {
                SupportsConstraints = true,
                SupportsRelations = true,
                SupportsLookupConditions = false,
                SupportsSequences = true,
                KnownActionType = KnownActions.Query,
                SupportsBulk = false,
                Name = KnownActions.Query.ToString(),
                FullName = KnownActions.Query.ToString(),
                Description = string.Empty

            };

            actionDefinitions.Add(queryDef);

            return actionDefinitions;
        }

        public IMethodDefinition RetrieveMethodDefinition(string objectName, bool shouldGetParameters = false)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<IMethodDefinition> RetrieveMethodDefinitions(bool shouldGetParameters = false)
        {
            throw new NotImplementedException();
        }

        public IObjectDefinition RetrieveObjectDefinition(string objectName, bool shouldGetProperties = false, bool shouldGetRelations = false)
        {
            IObjectDefinition objectDefinition = null;
            //extract the one type that Scribe Online is asking for: 
            if (EntityCollection.Count > 0)
            {
                foreach (var keyValuePair in EntityCollection)
                {
                    if (keyValuePair.Key == objectName)
                    {
                        Type entityType = keyValuePair.Value;
                        if (entityType != null)
                        {
                            //hand the type down to our reflection method and create an IObjectDefiniton for Scribe Online
                            objectDefinition = GetObjectDefinition(entityType, true);
                        }
                    }
                }
            }

            return objectDefinition;
        }

        public IEnumerable<IObjectDefinition> RetrieveObjectDefinitions(bool shouldGetProperties = false, bool shouldGetRelations = false)
        {
            foreach (var entityType in EntityCollection)
            {
                yield return RetrieveObjectDefinition(entityType.Key, shouldGetProperties, shouldGetRelations);
            }
        }

        public void Dispose()
        {
        }

        #endregion

        #region Private Helper Methods

        /// <summary>
        /// Uses reflection to generate Metadata from the in-memory entities.
        /// </summary>
        /// <param name="entityType">The entity type from which to create metadata.</param>
        /// <param name="shouldGetFields">Whether or not this method should get the entity's fields.</param>
        /// <returns>A single IObjectDefinition that represents the entity</returns>
        private IObjectDefinition GetObjectDefinition(Type entityType, bool shouldGetFields)
        {

            IObjectDefinition objectDefinition = null;

            objectDefinition = new ObjectDefinition
            {
                Name = entityType.Name,
                FullName = entityType.Name,
                Description = string.Empty,
                Hidden = false,
                RelationshipDefinitions = new List<IRelationshipDefinition>(),
                PropertyDefinitions = new List<IPropertyDefinition>(),
                SupportedActionFullNames = new List<string>()
            };

            objectDefinition.SupportedActionFullNames.Add("Query");
            if (entityType.Name == "Education"|| entityType.Name == "Candidate" ||entityType.Name == "WorkHistory")
            {
                objectDefinition.SupportedActionFullNames.Add("Create");
                objectDefinition.SupportedActionFullNames.Add("UpdateInsert");
            }
            if (shouldGetFields)
            {
                objectDefinition.PropertyDefinitions = GetFieldDefinitions(entityType);
            }
            return objectDefinition;

        }

        /// <summary>
        /// Uses reflection to pull the Fields from the Entities for Scribe OnLine
        /// </summary>
        /// <param name="entityType"></param>
        /// <returns></returns>
        private List<IPropertyDefinition> GetFieldDefinitions(Type entityType)
        {

            var fields = new List<IPropertyDefinition>();

            //Pull a collection from the incoming entity:
            var fieldsFromType = entityType.GetProperties(BindingFlags.Instance | BindingFlags.FlattenHierarchy |
                BindingFlags.Public | BindingFlags.GetProperty);

            foreach (var field in fieldsFromType)
            {

                var propertyDefinition = new PropertyDefinition
                {
                    Name = field.Name,
                    FullName = field.Name,
                    PropertyType = field.PropertyType.ToString(),
                    PresentationType = field.PropertyType.ToString(),
                    Nullable = false,
                    IsPrimaryKey = false,
                    UsedInQueryConstraint = true,
                    UsedInQuerySelect = true,
                    UsedInActionOutput = true,
                    UsedInQuerySequence = true,
                    Description = field.Name,
                    UsedInActionInput = true
                };

                //Find any of the following fields that may be attached to the entity. 
                //These are defined by the attributes attached to the property
                foreach (var attribute in field.GetCustomAttributes(false))
                {

                    //whether the field is readonly or not
                   /* if (attribute is ReadOnlyAttribute)
                    {
                        var readOnly = (ReadOnlyAttribute)attribute;
                        propertyDefinition.UsedInActionInput = readOnly == null || !readOnly.IsReadOnly;
                    }*/

                    //whether the field is required to be populated on an insert
                    if (attribute is RequiredAttribute)
                    {
                        propertyDefinition.RequiredInActionInput = true;
                    }

                    //if the field can be used as a match field for the query
                    if (attribute is KeyAttribute)
                    {
                        propertyDefinition.UsedInLookupCondition = true;
                    }

                }

                fields.Add(propertyDefinition);

            }

            return fields;

        }

        /// <summary>
        /// Fills a dictionary with the entities we want to hand back to Scribe Online. 
        /// </summary>
        /// <returns></returns>
        private Dictionary<string, Type> PopulateEntityCollection()
        {
            Dictionary<string, Type> entities = new Dictionary<string, Type>();
          
            entities.Add(CANDIDATE_ENTITY, typeof(Candidate));
            entities.Add(EDUCATION_ENTITY, typeof(Education));
            entities.Add(WORKHISTORY_ENTITY, typeof(WorkHistory));
            return entities;

        }
        

        #endregion
    }
}
