﻿using Scribe.Core.ConnectorApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Scribe.Core.ConnectorApi.Actions;
using Scribe.Core.ConnectorApi.Query;
using Scribe.Core.ConnectorApi.ConnectionUI;
using System.Collections.ObjectModel;
using Scribe.Core.ConnectorApi.Exceptions;

namespace TBEConnector
{
    [ScribeConnector(
        ConnectorSettings.ConnectorTypeId,
        ConnectorSettings.Name,
        ConnectorSettings.Description,
        typeof(Connector),
        StandardConnectorSettings.SettingsUITypeName,
        StandardConnectorSettings.SettingsUIVersion,
        StandardConnectorSettings.ConnectionUITypeName,
        StandardConnectorSettings.ConnectionUIVersion,
        StandardConnectorSettings.XapFileName,
        new[] { "Scribe.IS.Source", "Scribe.IS.Target", "Scribe.IS2.Source", "Scribe.IS2.Target" },
        ConnectorSettings.SupportsCloud,
        ConnectorSettings.ConnectorVersion
        )]
    public class Connector : IConnector
    {
        TBEClient _TBEClient = new TBEClient();
        QueryProcessor _queryProcessor = new QueryProcessor();
        public Guid ConnectorTypeId
        {
            get
            {
                return new Guid(ConnectorSettings.ConnectorTypeId);
            }
        }

        public bool IsConnected
        {
            get;
            set;
        }

        public void Connect(IDictionary<string, string> properties)
        {
            try
            {
                string userName = properties["userName"];
                string password = properties["password"];
                string companyCode = properties["comapnyCode"];
                _TBEClient.CheckLoginUrlAndAccessToken(companyCode , userName , password);
                this.IsConnected = true;
            }
            catch(Exception exception)
            {
                _TBEClient.GetLogOut();
                this.IsConnected = false;
                var message = string.Format("{0} {1}", "CompanyCode or User name or Password is incorrect", exception.Message);
                throw new Exception(message);
            }
        }

        public void Disconnect()
        {
            _TBEClient.GetLogOut();
            this.IsConnected = false;
        }

        public MethodResult ExecuteMethod(MethodInput input)
        {
            throw new NotImplementedException();
        }

        public OperationResult ExecuteOperation(OperationInput input)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<DataEntity> ExecuteQuery(Query query)
        {
            IEnumerable<DataEntity> entities = null;
            try
            {
                entities = _queryProcessor.ExecuteQuery(query);
            }
            catch(Exception exception)
            {
                var message = string.Format("{0} {1}", "Adapter Error", exception.Message);
                throw new InvalidExecuteQueryException(message);
            }
            return entities;
        }
        MetadataProvider _metadataProvider = new MetadataProvider();
        public IMetadataProvider GetMetadataProvider()
        {
            return _metadataProvider;
        }

        public string PreConnect(IDictionary<string, string> properties)
        {
            var form = new FormDefinition
            {
                CompanyName = "TBE",
                HelpUri = new Uri("http://icimshub.force.com/customer/s/"),
                CryptoKey = "1",
                Entries = new Collection<EntryDefinition>
                {
                    new EntryDefinition
                    {
                        InputType = InputType.Text,
                        IsRequired = true,
                        Label = "User Name",
                        PropertyName = "userName"
                    },
                    new EntryDefinition
                     {
                        InputType = InputType.Text,
                        IsRequired = true,
                        Label = "Password",
                        PropertyName = "password"
                    },
                    new EntryDefinition

                    {
                        InputType = InputType.Text,
                        IsRequired = true,
                        Label = "Comapny Code",
                        PropertyName = "comapnyCode"
                    }
                }
            };
            return form.Serialize();
        }
    }
}
