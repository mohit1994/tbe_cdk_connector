﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TBEConnector.models;

namespace TBEConnector
{
    class HierarchicalToFlattenedPersonConverter
    {
        public static Candidate ConvertHierarchicalEntityToFlattenedEntity(CandidateHierarchicalModel candidateHierarchicalModel)
        {
            Candidate candidateFlatModel = new Candidate();
            try
            {
                if (candidateHierarchicalModel != null)
                {
                    candidateFlatModel.candAcceptedOfferPDF = candidateHierarchicalModel.candAcceptedOfferPDF;
                    candidateFlatModel.candAcceptedOfferId = candidateHierarchicalModel.candAcceptedOfferId;
                    candidateFlatModel.creationDate = candidateHierarchicalModel.creationDate;
                    candidateFlatModel.referredById = candidateHierarchicalModel.referredById;
                    candidateFlatModel.hrnxorderstatus = candidateHierarchicalModel.hrnxorderstatus;
                    candidateFlatModel.candReqAce = candidateHierarchicalModel.candReqAce;
                    candidateFlatModel.cc305PDFCandidate = candidateHierarchicalModel.cc305PDFCandidate;
                    candidateFlatModel.cws = candidateHierarchicalModel.cws;
                    candidateFlatModel.IndividualWithDisability = candidateHierarchicalModel.IndividualWithDisability;
                    candidateFlatModel.candStartStatus = candidateHierarchicalModel.candStartStatus;
                    candidateFlatModel.city = candidateHierarchicalModel.city;
                    candidateFlatModel.country = candidateHierarchicalModel.country;
                    candidateFlatModel.county = candidateHierarchicalModel.county;
                    candidateFlatModel.resumeText = candidateHierarchicalModel.resumeText;
                    candidateFlatModel.CurrentSalary = candidateHierarchicalModel.CurrentSalary;
                    candidateFlatModel.birthdate = candidateHierarchicalModel.birthdate;
                    candidateFlatModel.email = candidateHierarchicalModel.email;
                    candidateFlatModel.employee = candidateHierarchicalModel.employee;
                    candidateFlatModel.firstName = candidateHierarchicalModel.firstName;
                    candidateFlatModel.flagged = candidateHierarchicalModel.flagged;
                    candidateFlatModel.gender = candidateHierarchicalModel.gender;
                    candidateFlatModel.googleSearch = candidateHierarchicalModel.googleSearch;
                    candidateFlatModel.educationLevel = candidateHierarchicalModel.educationLevel;
                    candidateFlatModel.hiredDate = candidateHierarchicalModel.hiredDate;
                    candidateFlatModel.hiredForReqDepartment = candidateHierarchicalModel.hiredForReqDepartment;
                    candidateFlatModel.hiredForReqId = candidateHierarchicalModel.hiredForReqId;
                    candidateFlatModel.hiredForReqJobCode = candidateHierarchicalModel.hiredForReqJobCode;
                    candidateFlatModel.hiredForReqLocation = candidateHierarchicalModel.hiredForReqLocation;
                    candidateFlatModel.hiredForReqTitle = candidateHierarchicalModel.hiredForReqTitle;
                    candidateFlatModel.candId = candidateHierarchicalModel.candId;
                    candidateFlatModel.inStatusDate = candidateHierarchicalModel.inStatusDate;
                    candidateFlatModel.lastName = candidateHierarchicalModel.lastName;
                    candidateFlatModel.lastUpdated = candidateHierarchicalModel.lastUpdated;
                    candidateFlatModel.licenseNumber = candidateHierarchicalModel.licenseNumber;
                    candidateFlatModel.linkedInSearch = candidateHierarchicalModel.linkedInSearch;
                    candidateFlatModel.ReasonRej = candidateHierarchicalModel.ReasonRej;
                    candidateFlatModel.status = candidateHierarchicalModel.status;
                    candidateFlatModel.maritalStatus = candidateHierarchicalModel.maritalStatus;
                    candidateFlatModel.rank = candidateHierarchicalModel.rank;
                    candidateFlatModel.middleInitial = candidateHierarchicalModel.middleInitial;
                    candidateFlatModel.cellPhone = candidateHierarchicalModel.cellPhone;
                    candidateFlatModel.OtherSpecifySource = candidateHierarchicalModel.OtherSpecifySource;
                    candidateFlatModel.passportNumber = candidateHierarchicalModel.passportNumber;
                    candidateFlatModel.cwsPassword = candidateHierarchicalModel.cwsPassword;
                    candidateFlatModel.PaychexExported = candidateHierarchicalModel.PaychexExported;
                    candidateFlatModel.phone = candidateHierarchicalModel.phone;
                    candidateFlatModel.preferredLocale = candidateHierarchicalModel.preferredLocale;
                    candidateFlatModel.candidatePicture = candidateHierarchicalModel.candidatePicture;
                    candidateFlatModel.race = candidateHierarchicalModel.race;
                    candidateFlatModel.referredBy = candidateHierarchicalModel.referredBy;
                    candidateFlatModel.religion = candidateHierarchicalModel.religion;
                    candidateFlatModel.salutation = candidateHierarchicalModel.salutation;
                    candidateFlatModel.ssn = candidateHierarchicalModel.ssn;
                    candidateFlatModel.source = candidateHierarchicalModel.source;
                    candidateFlatModel.startDate = candidateHierarchicalModel.startDate;
                    candidateFlatModel.state = candidateHierarchicalModel.state;
                    candidateFlatModel.address2 = candidateHierarchicalModel.address2;
                    candidateFlatModel.address = candidateHierarchicalModel.address;
                    candidateFlatModel.nameSuffix = candidateHierarchicalModel.nameSuffix;
                    candidateFlatModel.veteran = candidateHierarchicalModel.veteran;
                    candidateFlatModel.legalStatus = candidateHierarchicalModel.legalStatus;
                    candidateFlatModel.zipCode = candidateHierarchicalModel.zipCode;
                    candidateFlatModel.resumeFileName = candidateHierarchicalModel.resumeFileName;
                    candidateFlatModel.resumeContentType = candidateHierarchicalModel.resumeContentType;


                    SetRelationshipurls(candidateFlatModel, candidateHierarchicalModel);

                }
            }
            catch
            {

            }
            return candidateFlatModel;
        }


        private static void SetRelationshipurls(Candidate candidateFlatModel, CandidateHierarchicalModel candidateHierarchicalModel)
        {
            try
            {
                if (candidateHierarchicalModel != null)
                {
                    candidateFlatModel.requisition = candidateHierarchicalModel.relationshipUrls.requisition;
                    candidateFlatModel.attachments = candidateHierarchicalModel.relationshipUrls.attachments;
                    candidateFlatModel.resume = candidateHierarchicalModel.relationshipUrls.resume;
                    candidateFlatModel.interview = candidateHierarchicalModel.relationshipUrls.interview;
                    candidateFlatModel.workhistory = candidateHierarchicalModel.relationshipUrls.workhistory;
                    candidateFlatModel.reference = candidateHierarchicalModel.relationshipUrls.reference;
                    candidateFlatModel.education = candidateHierarchicalModel.relationshipUrls.education;
                    candidateFlatModel.residence = candidateHierarchicalModel.relationshipUrls.residence;
                    candidateFlatModel.certificate = candidateHierarchicalModel.relationshipUrls.certificate;
                    candidateFlatModel.backgroundcheck = candidateHierarchicalModel.relationshipUrls.backgroundcheck;
                    candidateFlatModel.offer = candidateHierarchicalModel.relationshipUrls.offer;
                    candidateFlatModel.expense = candidateHierarchicalModel.relationshipUrls.expense;
                    candidateFlatModel.comment = candidateHierarchicalModel.relationshipUrls.comment;
                    candidateFlatModel.historylog = candidateHierarchicalModel.relationshipUrls.comment;
                    candidateFlatModel.contactlog = candidateHierarchicalModel.relationshipUrls.comment;


                }
            }

            catch
            {

            }

           
        }
    }
}
