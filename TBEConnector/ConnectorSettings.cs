﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TBEConnector
{
    public class ConnectorSettings
    {
        //mohit guid
        //   public const string ConnectorTypeId = "{39B92CFB-1716-4ABE-AEAD-3CB82333D847}";
        //only for test kamini guid
        public const string ConnectorTypeId = "{20283C6C-8F98-4CDA-8FC5-EB553641A7B3}";
        public const string ConnectorVersion = "1.0";
        public const string Description = "Provided by HRNX";
        //public const string Name = "TBEConnector";
        //only for check by kamini
        public const string Name = "Kamini_TBEConnector";
        public const bool SupportsCloud = false;

    }
}
