﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TBEConnector
{
    public class Education
    {
        public string eduHistCity { get; set; }
        public string eduHistFrom { get; set; }
        public string eduHistTo { get; set; }
        public string eduHistDegreeAchieved { get; set; }
        public string eduHistDegree { get; set; }
        public string eduHistFieldOfStudy { get; set; }
        public string eduHistSchool { get; set; }
        public string eduHistState { get; set; }
        public int id { get; set; }
    }


}
