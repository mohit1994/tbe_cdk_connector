﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TBEConnector.models
{
    public class WorkHistory
    {
        public string workHistCityState { get; set; }
        public string workHistCoName { get; set; }
        public string workHistPhone { get; set; }
        public string workHistStreet { get; set; }
        public string workHistFrom { get; set; }
        public string workHistTo { get; set; }
        public string workHistDesc { get; set; }
        public string workHistSupervisor { get; set; }
        public string workHistExplanation { get; set; }
        public string workHistPay { get; set; }
        public bool workHistOkContact { get; set; }
        public string workHistReasonForLeaving { get; set; }
        public string StartingRateOfPay { get; set; }
        public string workHistSuperTitle { get; set; }
        public string workHistJobTitle { get; set; }
        public int id { get; set; }
    }

}
