﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TBEConnector.models
{
    public class UrlResponseData
    {
        public Response response { get; set; }
    }
    public class Response
    {
        public string URL { get; set; }
        public string authToken { get; set; }
    }
}
