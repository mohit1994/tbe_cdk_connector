﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TBEConnector.models;

namespace TBEConnector
{
    class FlattenedToHierarchicalPersonConverter
    {

        public static CandidateHierarchicalModel ConvertFlatEntityCandidateToCandidateRequestEntity(Candidate candidateFlatModel)
        {
            CandidateHierarchicalModel candidateHierarchicalModel = new CandidateHierarchicalModel();
            try {
                if (candidateHierarchicalModel != null) {
                    candidateHierarchicalModel.candAcceptedOfferPDF = candidateFlatModel.candAcceptedOfferPDF;
                    candidateHierarchicalModel.candAcceptedOfferId = candidateFlatModel.candAcceptedOfferId;
                    candidateHierarchicalModel.creationDate = candidateFlatModel.creationDate;
                    candidateHierarchicalModel.referredById = candidateFlatModel.referredById;
                    candidateHierarchicalModel.hrnxorderstatus = candidateFlatModel.hrnxorderstatus;
                    candidateHierarchicalModel.candReqAce = candidateFlatModel.candReqAce;
                    candidateHierarchicalModel.cc305PDFCandidate = candidateFlatModel.cc305PDFCandidate;
                    candidateHierarchicalModel.cws = candidateFlatModel.cws;
                    candidateHierarchicalModel.IndividualWithDisability = candidateFlatModel.IndividualWithDisability;
                    candidateHierarchicalModel.candStartStatus = candidateFlatModel.candStartStatus;
                    candidateHierarchicalModel.city = candidateFlatModel.city;
                    candidateHierarchicalModel.country = candidateFlatModel.country;
                    candidateHierarchicalModel.county = candidateFlatModel.county;
                    candidateHierarchicalModel.resumeText = candidateFlatModel.resumeText;
                    candidateHierarchicalModel.CurrentSalary = candidateFlatModel.CurrentSalary;
                    candidateHierarchicalModel.birthdate = candidateFlatModel.birthdate;
                    candidateHierarchicalModel.email = candidateFlatModel.email;
                    candidateHierarchicalModel.employee = candidateFlatModel.employee;
                    candidateHierarchicalModel.firstName = candidateFlatModel.firstName;
                    candidateHierarchicalModel.flagged = candidateFlatModel.flagged;
                    candidateHierarchicalModel.gender = candidateFlatModel.gender;
                    candidateHierarchicalModel.googleSearch = candidateFlatModel.googleSearch;
                    candidateHierarchicalModel.educationLevel = candidateFlatModel.educationLevel;
                    candidateHierarchicalModel.hiredDate = candidateFlatModel.hiredDate;
                    candidateHierarchicalModel.hiredForReqDepartment = candidateFlatModel.hiredForReqDepartment;
                    candidateHierarchicalModel.hiredForReqId = candidateFlatModel.hiredForReqId;
                    candidateHierarchicalModel.hiredForReqJobCode = candidateFlatModel.hiredForReqJobCode;
                    candidateHierarchicalModel.hiredForReqLocation = candidateFlatModel.hiredForReqLocation;
                    candidateHierarchicalModel.hiredForReqTitle = candidateFlatModel.hiredForReqTitle;
                    candidateHierarchicalModel.candId = candidateFlatModel.candId;
                    candidateHierarchicalModel.inStatusDate = candidateFlatModel.inStatusDate;
                    candidateHierarchicalModel.lastName = candidateFlatModel.lastName;
                    candidateHierarchicalModel.lastUpdated = candidateFlatModel.lastUpdated;
                    candidateHierarchicalModel.licenseNumber = candidateFlatModel.licenseNumber;
                    candidateHierarchicalModel.linkedInSearch = candidateFlatModel.linkedInSearch;
                    candidateHierarchicalModel.ReasonRej = candidateFlatModel.ReasonRej;
                    candidateHierarchicalModel.status = candidateFlatModel.status;
                    candidateHierarchicalModel.maritalStatus = candidateFlatModel.maritalStatus;
                    candidateHierarchicalModel.rank = candidateFlatModel.rank;
                    candidateHierarchicalModel.middleInitial = candidateFlatModel.middleInitial;
                    candidateHierarchicalModel.cellPhone = candidateFlatModel.cellPhone;
                    candidateHierarchicalModel.OtherSpecifySource = candidateFlatModel.OtherSpecifySource;
                    candidateHierarchicalModel.passportNumber = candidateFlatModel.passportNumber;
                    candidateHierarchicalModel.cwsPassword = candidateFlatModel.cwsPassword;
                    candidateHierarchicalModel.PaychexExported = candidateFlatModel.PaychexExported;
                    candidateHierarchicalModel.phone = candidateFlatModel.phone;
                    candidateHierarchicalModel.preferredLocale = candidateFlatModel.preferredLocale;
                    candidateHierarchicalModel.candidatePicture = candidateFlatModel.candidatePicture;
                    candidateHierarchicalModel.race = candidateFlatModel.race;
                    candidateHierarchicalModel.referredBy = candidateFlatModel.referredBy;
                    candidateHierarchicalModel.religion = candidateFlatModel.religion;
                    candidateHierarchicalModel.salutation = candidateFlatModel.salutation;
                    candidateHierarchicalModel.ssn = candidateFlatModel.ssn;
                    candidateHierarchicalModel.source = candidateFlatModel.source;
                    candidateHierarchicalModel.startDate = candidateFlatModel.startDate;
                    candidateHierarchicalModel.state = candidateFlatModel.state;
                    candidateHierarchicalModel.address2 = candidateFlatModel.address2;
                    candidateHierarchicalModel.address = candidateFlatModel.address;
                    candidateHierarchicalModel.nameSuffix = candidateFlatModel.nameSuffix;
                    candidateHierarchicalModel.veteran = candidateFlatModel.veteran;
                    candidateHierarchicalModel.legalStatus = candidateFlatModel.legalStatus;
                    candidateHierarchicalModel.zipCode = candidateFlatModel.zipCode;
                    candidateHierarchicalModel.resumeFileName = candidateFlatModel.resumeFileName;
                    candidateHierarchicalModel.resumeContentType = candidateFlatModel.resumeContentType;
                    candidateHierarchicalModel.relationshipUrls = GetRelationshipurls(candidateFlatModel);
                }
            }

            catch
            {

            }
            return candidateHierarchicalModel;
        }

        private static Relationshipurls GetRelationshipurls(Candidate candidateFlatModel)
        {
            CandidateHierarchicalModel candidateHierarchicalModel = new CandidateHierarchicalModel();
            try {
                if (candidateHierarchicalModel.relationshipUrls != null)
                {
                    candidateHierarchicalModel.relationshipUrls.requisition = candidateFlatModel.requisition;
                    candidateHierarchicalModel.relationshipUrls.attachments = candidateFlatModel.attachments;
                    candidateHierarchicalModel.relationshipUrls.resume = candidateFlatModel.resume;
                    candidateHierarchicalModel.relationshipUrls.interview = candidateFlatModel.interview;
                    candidateHierarchicalModel.relationshipUrls.workhistory = candidateFlatModel.workhistory;
                    candidateHierarchicalModel.relationshipUrls.reference = candidateFlatModel.reference;
                    candidateHierarchicalModel.relationshipUrls.education = candidateFlatModel.education;
                    candidateHierarchicalModel.relationshipUrls.residence = candidateFlatModel.residence;
                    candidateHierarchicalModel.relationshipUrls.certificate = candidateFlatModel.certificate;
                    candidateHierarchicalModel.relationshipUrls.backgroundcheck = candidateFlatModel.backgroundcheck;
                    candidateHierarchicalModel.relationshipUrls.offer = candidateFlatModel.offer;
                    candidateHierarchicalModel.relationshipUrls.expense = candidateFlatModel.expense;
                    candidateHierarchicalModel.relationshipUrls.comment = candidateFlatModel.comment;
                    candidateHierarchicalModel.relationshipUrls.historylog = candidateFlatModel.historylog;
                    candidateHierarchicalModel.relationshipUrls.contactlog = candidateFlatModel.contactlog;
                }
            }

            catch
            {

            }
            return candidateHierarchicalModel.relationshipUrls;
        }
    }
}
