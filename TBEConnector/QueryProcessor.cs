﻿using Scribe.Core.ConnectorApi.Query;
using Scribe.Core.ConnectorApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace TBEConnector
{
    public class QueryProcessor
    {
        public IEnumerable<DataEntity> ExecuteQuery(Query query)
        {
            IEnumerable<DataEntity> results = new List<DataEntity>();
            string entityName = query.RootEntity.ObjectDefinitionFullName;
            switch (entityName)
            {
                case "Candidate":
                     break;
                case "Education":
                    break;
                case "WorkHistory":
                    break;
                default:
                    break;
            }
            return results;
        }



        private IEnumerable<DataEntity> ToDataEntity<T>(IEnumerable<T> entities)
        {
            var type = typeof(T);
            var fields = type.GetProperties(BindingFlags.Instance |
                BindingFlags.FlattenHierarchy |
                BindingFlags.Public |
                BindingFlags.GetProperty);
            foreach (var entity in entities)
            {
                var dataEntity = new QueryDataEntity
                {
                    ObjectDefinitionFullName = type.Name,
                    Name = type.Name
                };
                foreach (var field in fields)
                {
                    dataEntity.Properties.Add(
                        field.Name,
                        field.GetValue(entity, null));
                }
                yield return dataEntity.ToDataEntity();
            }

        }

    }
    
}
